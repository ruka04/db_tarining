SELECT DATE_FORMAT(SaleDate, '%Y-%m') as SalesMonth, SUM(Quantity*Price) as SalesAmount
FROM Sales
JOIN Products ON Sales.ProductID = Products.ProductID
GROUP BY SalesMonth
;