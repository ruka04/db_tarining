SELECT Employees.EmployeeID,EmployeeName,DATE_FORMAT(SaleDate, '%Y-%m') as SalesMonth,SUM(Quantity*Price) as SalesAmount
FROM Employees
LEFT JOIN Sales ON Employees.EmployeeID = Sales.EmployeeID
LEFT JOIN Products ON Sales.ProductID = Products.ProductID
GROUP BY Employees.EmployeeID,SalesMonth
ORDER BY Employees.EmployeeID,EmployeeName,SalesMonth
;