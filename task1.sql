-- 問題文の顧客、商品別という点に注目してください　言い換えると同一の顧客、商品については集約されていないといけないということです
SELECT Customers.CustomerID,CustomerName,ProductName,SUM(Price*Quantity) AS SalesAmount
FROM Sales
LEFT JOIN Products ON Products.ProductID = Sales.ProductID
LEFT JOIN Customers ON Sales.CustomerID = Customers.CustomerID
GROUP BY Customers.CustomerID,Products.ProductID
ORDER BY Customers.CustomerID,CustomerName,ProductName
;