SELECT Products.ProductID,ProductName,DATE_FORMAT(SaleDate, '%Y-%m') as SalesMonth,SUM(Quantity*Price) as SalesAmount
FROM Products
JOIN Sales ON Sales.ProductID = Products.ProductID
GROUP BY Products.ProductID,ProductName,SalesMonth,CategoryID
HAVING SalesAmount >= 5000 AND (CategoryID = 3 OR CategoryID = 1 OR CategoryID = 9)
;