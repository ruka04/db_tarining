SELECT Departments.DepartmentID,DepartmentName,DATE_FORMAT(PayDate, '%Y-%m') as PayMonth,ROUND(AVG(Amount), 0)
FROM Departments
JOIN BelongTo ON Departments.DepartmentID = BelongTo.DepartmentID
JOIN Salary ON BelongTo.EmployeeID = Salary.EmployeeID
WHERE (PayDate BETWEEN '2007-04-01' AND '2008/03/31') AND (EndDate IS NULL OR PayDate BETWEEN StartDate AND EndDate)
GROUP BY Departments.DepartmentID,DepartmentName,PayMonth
;